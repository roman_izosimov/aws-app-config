﻿namespace AwsAppConfigClient
{
    public class Secrets
    {
        public string AwsKey { get; }

        public string AwsSecret { get; }

        public Secrets(string awsKey, string awsSecret)
        {
            AwsKey = awsKey;
            AwsSecret = awsSecret;
        }

        //public string AwsKey = "AKIAR4TFTOQTZCIEWAEU";
        //public string AwsSecret = "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";
    }
}